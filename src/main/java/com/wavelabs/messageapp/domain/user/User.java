package com.wavelabs.messageapp.domain.user;


import com.wavelabs.messageapp.domain.model.contacts.Contact;
import com.wavelabs.messageapp.domain.shared.Entity;

public class User implements Entity<User>{
	private UserId id;
	private Mobile mobile;
	private Contact contact;

	public User() {
	}

	public User(final UserId id, final Mobile mobile) {
		super();
		this.id = id;
		this.mobile = mobile;
	}

	@SuppressWarnings("unused")
	private UserId getId() {
		return id;
	}

	@SuppressWarnings("unused")
	private Mobile getMobile() {
		return mobile;
	}
	public Contact contact(){
		return contact;
	}

public void addContact(final Contact contact){
	 this.contact=contact;
}

public void deleteContact(final Contact contact){
	this.contact = contact;
}

@Override
public boolean sameIdentityAs(User other) {
	return false;
}
}
