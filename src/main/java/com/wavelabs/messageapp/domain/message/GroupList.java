package com.wavelabs.messageapp.domain.message;

import com.wavelabs.messageapp.domain.shared.Entity;

public class GroupList implements Entity<GroupList> {
	private GroupListId id;
	private GroupingId gid;
	private UserId mid;

	public GroupList(GroupListId id, GroupingId gid, UserId mid) {
		super();
		this.id = id;
		this.gid = gid;
		this.mid = mid;
	}

	public GroupListId getId() {
		return id;
	}

	public GroupingId getGid() {
		return gid;
	}

	public UserId getMid() {
		return mid;
	}

	@Override
	public boolean sameIdentityAs(GroupList other) {
		return false;
	}

}
