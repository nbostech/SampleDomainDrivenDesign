package com.wavelabs.messagapp.model.entities.events;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "UserHandler")
public class UserHandler {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private int id;
	@Column(name = "Description")
	private String description;
	@Column(name = "Type")
	@Enumerated(EnumType.STRING )
	private EventType type;
	@Column(name = "TimeStamp")
	private Calendar timeStamp;
	@Column(name = "Status")
	@Enumerated(EnumType.STRING)
	private EventStatus status;

	public UserHandler() {

	}

	

	public UserHandler(int id, String description, EventType type, Calendar timeStamp, EventStatus status) {
		super();
		this.id = id;
		this.description = description;
		this.type = type;
		this.timeStamp = timeStamp;
		this.status = status;
	}



	public EventType getType() {
		return type;
	}



	public void setType(EventType type) {
		this.type = type;
	}



	public EventStatus getStatus() {
		return status;
	}

	public void setStatus(EventStatus status) {
		this.status = status;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Calendar getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(Calendar timeStamp) {
		this.timeStamp = timeStamp;
	}

	
}
	
