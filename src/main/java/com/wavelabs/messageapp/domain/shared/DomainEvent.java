package com.wavelabs.messageapp.domain.shared;

public interface DomainEvent<T> {
	 boolean sameEventAs(T other);
}
