This is the sample Domain Driven Design Example.

Domain Driven Design:
Domain is nothing but problem, we need to provide the solution in the form of design. So that the
Business Experts can understand the solution for a particular problem. For that we should use Ubiquitous language.
It is a language for easy communication between Domain experts, Technical experts, Developers etc.

In this example, User and Message are sub-domains. 
User is an aggregate. It has entity like Contact.
Message is an aggregate. It has entities like Group, GroupList.

Features of User:
adding a contact.
deleting a contact.

Features of Message:
sending a message to a targeted person.
creating a group and sending a message in group.


Aggregates:
User
Message

Entities:
Contact
Group
GroupList

Events:
contactCreated
contactDeleted
GroupCreated
MessageCreated
GroupMessageCreated