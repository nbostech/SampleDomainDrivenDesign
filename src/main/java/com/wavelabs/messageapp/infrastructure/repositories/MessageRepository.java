package com.wavelabs.messageapp.infrastructure.repositories;


import org.springframework.data.jpa.repository.JpaRepository;

import com.wavelabs.messageapp.domain.model.message.Message;

public interface MessageRepository extends JpaRepository<Message, Integer>{

	Message findMessageById(int id);



	




}
