package com.wavelabs.messageapp.infrastructure.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.wavelabs.messagapp.model.entities.events.UserHandler;
@Repository
public interface UserHandlerRepository extends CrudRepository<UserHandler, Integer>{

}
