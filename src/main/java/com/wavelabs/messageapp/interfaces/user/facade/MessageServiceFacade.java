package com.wavelabs.messageapp.interfaces.user.facade;

import com.wavelabs.messageapp.domain.model.group.Grouping;
import com.wavelabs.messageapp.domain.model.message.Message;

public interface MessageServiceFacade {
	public boolean messageCreated(Message message);
	public boolean groupmessageCreated(Message message);
	public boolean groupCreated(Grouping grouping);

}
