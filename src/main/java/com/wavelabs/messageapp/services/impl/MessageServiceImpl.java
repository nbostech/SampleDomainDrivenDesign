package com.wavelabs.messageapp.services.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wavelabs.messageapp.domain.model.group.GroupList;
import com.wavelabs.messageapp.domain.model.group.Grouping;
import com.wavelabs.messageapp.domain.model.message.Message;
import com.wavelabs.messageapp.infrastructure.repositories.GroupListRepository;
import com.wavelabs.messageapp.infrastructure.repositories.GroupRepository;
import com.wavelabs.messageapp.infrastructure.repositories.MessageRepository;
import com.wavelabs.messageapp.services.MessageService;

@Service
public class MessageServiceImpl implements MessageService {
	static Logger log = Logger.getLogger(MessageServiceImpl.class);
	@Autowired
	MessageService messageService;
	@Autowired
	MessageRepository messageRepo;
	@Autowired
	GroupRepository groupRepo;
	@Autowired
	GroupListRepository groupListRepo;

	@Override
	public Boolean persistMessage(Message message) {
		try {
			messageRepo.save(message);

			return true;
		} catch (Exception e) {
			log.info(e);
			return false;
		}

	}

	@Override
	public Boolean persistGroup(Grouping group) {
		try {
			groupRepo.save(group);
			return true;
		} catch (Exception e) {
			log.info(e);
			return false;
		}
	}

	@Override
	public Boolean peristGroups(GroupList groupList) {
		try {
			groupListRepo.save(groupList);
			return true;
		} catch (Exception e) {
			log.info(e);
			return null;
		}
	}

	@Override
	public Boolean persistMessageInGroup(Message message) {
		try {
			messageRepo.save(message);

			return true;
		} catch (Exception e) {
			log.info(e);
			return false;
		}

	}

}
