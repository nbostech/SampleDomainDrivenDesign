package com.wavelabs.messageapp.services;

import com.wavelabs.messagapp.model.entities.events.MessageHandler;

public interface MessageEventService {
	public boolean createMessage(MessageHandler event);
	 public boolean createMessageInGroup(MessageHandler event);
	 public boolean createGroup(MessageHandler event);
}
