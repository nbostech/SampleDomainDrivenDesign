package com.wavelabs.messageapp.services;


import com.wavelabs.messageapp.domain.model.group.GroupList;
import com.wavelabs.messageapp.domain.model.group.Grouping;
import com.wavelabs.messageapp.domain.model.message.Message;

public interface MessageService {
	public Boolean persistMessage(Message message);
	public Boolean persistMessageInGroup(Message message);
	
	
	
	public Boolean persistGroup(Grouping group);
	
	
	public  Boolean peristGroups(GroupList groupList);
	

}
