package com.wavelabs.messagapp.model.entities.events;

import java.util.Calendar;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

@Component
public class UserHandlerFactory {
	Logger logger = Logger.getLogger(UserHandlerFactory.class);

	public UserHandler registerUserEvent(EventType type, EventStatus status) {
		UserHandler event = new UserHandler();
		event.setDescription("user created!");
		event.setStatus(EventStatus.success);
		event.setTimeStamp(Calendar.getInstance());
		event.setType(EventType.UserEvents);
		return event;
	}

	public UserHandler registerContactEvent(EventType type, EventStatus status) {
		UserHandler event = new UserHandler();
		event.setDescription("contact created!");
		event.setStatus(EventStatus.success);
		event.setTimeStamp(Calendar.getInstance());
		event.setType(EventType.UserEvents);
		return event;
	}

	public UserHandler registerContactEventForFailure(EventType type, EventStatus status) {
		UserHandler event = new UserHandler();
		event.setDescription("user creation failed!");
		event.setStatus(EventStatus.failure);
		event.setTimeStamp(Calendar.getInstance());
		event.setType(EventType.UserEvents);
		return event;
	}

	public UserHandler registerUserEventForFailure(EventType type, EventStatus status) {
		UserHandler event = new UserHandler();
		event.setDescription("user creation failed!");
		event.setStatus(EventStatus.failure);
		event.setTimeStamp(Calendar.getInstance());
		event.setType(EventType.UserEvents);
		return event;
	}

	public UserHandler deleteContactEvent(EventType type, EventStatus status) {
		UserHandler event = new UserHandler();
		event.setDescription("contact deleted!");
		event.setStatus(EventStatus.success);
		event.setTimeStamp(Calendar.getInstance());
		event.setType(EventType.UserEvents);
		return event;
	}

	public UserHandler deleteContactEventForFailure(EventType type, EventStatus status) {
		UserHandler event = new UserHandler();
		event.setDescription("contact deleted fails!");
		event.setStatus(EventStatus.failure);
		event.setTimeStamp(Calendar.getInstance());
		event.setType(EventType.UserEvents);
		return event;

	}

}
