package com.wavelabs.messageapp.domain.model.group;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.wavelabs.messageapp.domain.model.user.User;

@Entity
@Table(name = "Grouping")
public class Grouping {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private int id;
	@Column(name = "name")
	private String name;
	@ManyToOne(targetEntity = User.class, fetch = FetchType.EAGER, cascade = { CascadeType.MERGE, CascadeType.REFRESH })
	@JoinColumn(name = "uid")
	private User uid;

	public Grouping() {

	}

	public Grouping(int id, String name, User uid) {
		super();
		this.id = id;
		this.name = name;
		this.uid = uid;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public User getUid() {
		return uid;
	}

}
