package com.wavelabs.messageapp.infrastructure.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.wavelabs.messageapp.domain.model.group.GroupList;
import com.wavelabs.messageapp.domain.model.group.Grouping;

public interface GroupListRepository extends JpaRepository<GroupList, Integer>{

	

	Grouping findById(int gid);
	

}
