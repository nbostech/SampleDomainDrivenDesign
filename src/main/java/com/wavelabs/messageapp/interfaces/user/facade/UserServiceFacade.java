package com.wavelabs.messageapp.interfaces.user.facade;

import com.wavelabs.messageapp.domain.model.contacts.Contact;

public interface UserServiceFacade {


public boolean addNewContact(Contact contact);
public boolean deleteContact(int id);

  
}
