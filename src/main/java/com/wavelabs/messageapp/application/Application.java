package com.wavelabs.messageapp.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.wavelabs.messagapp.model.entities.events.UserHandler;
import com.wavelabs.messageapp.domain.message.Message;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * 
 * @author thejasreem
 *
 */

@EnableSwagger2
@Configuration
@EnableAutoConfiguration
@SpringBootApplication
@EnableJpaRepositories(basePackages = { "com.wavelabs.messageapp" })
@ComponentScan({ "com.wavelabs.messageapp", "com.wavelabs.messagapp.model.entities.events",
		"com.wavelabs.messageapp.domain.controllers" })
@EntityScan({ "com.wavelabs.messageapp", "com.wavelabs.messagapp.model.entities.events" })
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);

	}

	@Bean
	public Message message() {
		return new Message();
	}

	@Bean
	public UserHandler userHandling() {
		return new UserHandler();
	}

}