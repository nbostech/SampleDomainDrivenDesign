package com.wavelabs.messageapp.domain.model.message;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.wavelabs.messageapp.domain.model.group.Grouping;
import com.wavelabs.messageapp.domain.model.user.User;

/**
 * @author thejasreem
 *
 */
@Entity
@Table(name = "Message")
public class Message {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private int id;
	@ManyToOne
	@JoinColumn(name = "toId")
	private User toId;
	@ManyToOne
	@JoinColumn(name = "fromId")
	private User fromId;
	@ManyToOne
	@JoinColumn(name = "groupId")
	private Grouping gid;
	@Column(name = "TextMessage")
	private String text;

	public Message() {

	}

	public Message(int id, User toId, User fromId, Grouping gid, String text) {
		super();
		this.id = id;
		this.toId = toId;
		this.fromId = fromId;
		this.gid = gid;
		this.text = text;
	}

	public int getId() {
		return id;
	}

	public User getToId() {
		return toId;
	}

	public User getFromId() {
		return fromId;
	}

	public Grouping getGid() {
		return gid;
	}

	public String getText() {
		return text;
	}

}
