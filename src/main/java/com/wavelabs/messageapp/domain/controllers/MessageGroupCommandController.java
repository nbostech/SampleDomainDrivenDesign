/*package com.wavelabs.messageapp.domain.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.wavelabs.messagapp.model.entities.events.EventStatus;
import com.wavelabs.messagapp.model.entities.events.EventType;
import com.wavelabs.messagapp.model.entities.events.MessageHandler;
import com.wavelabs.messagapp.model.entities.events.MessageEventFactory;
import com.wavelabs.messageapp.domain.model.group.Grouping;
import com.wavelabs.messageapp.domain.model.message.Message;
import com.wavelabs.messageapp.services.MessageEventService;
import com.wavelabs.messageapp.services.MessageService;

import io.nbos.capi.api.v0.models.RestMessage;

@RestController
@Component
public class MessageGroupCommandController {
	@Autowired
	MessageService messageService;
	@Autowired
	MessageEventFactory messageFactory;
	@Autowired
	MessageHandler messageHandler;
	@Autowired
	MessageEventService messageEventService;
	RestMessage rm = new RestMessage();

	@SuppressWarnings("rawtypes")
	@RequestMapping(method = RequestMethod.POST, value = "/api/message")
	public ResponseEntity createNewMessageInGroup(@RequestBody Message message) {
		boolean flag = messageService.persistMessageInGroup(message);
		if (flag) {

			rm.message = "GroupMessage created successfully!!";
			rm.messageCode = "200";
			MessageHandler event = messageFactory.createMessageInGroup(EventType.MessageEvents, EventStatus.success);
			messageEventService.createMessageInGroup(event);
			return ResponseEntity.status(200).body(rm);
		} else {
			rm.message = "Message creation failed!!";
			rm.messageCode = "404";
			MessageHandler event = messageFactory.createMessageInGroupFailure(EventType.MessageEvents, EventStatus.failure);
			messageEventService.createMessageInGroup(event);
			return ResponseEntity.status(404).body(rm);

		}

	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(method = RequestMethod.POST, value = "/api/v1/group")
	public ResponseEntity createNewGroup(@RequestBody Grouping grouping) {
		boolean flag = messageService.persistGroup(grouping);
		if (flag) {
			rm.message = "Group created successfully!!";
			rm.messageCode = "200";
			MessageHandler event = messageFactory.createmessageEvent(EventType.MessageEvents, EventStatus.success);
			messageEventService.createGroup(event);
			return ResponseEntity.status(200).body(rm);
		} else {
			rm.message = "Group creation failed!!";
			rm.messageCode = "404";
			MessageHandler event = messageFactory.createGroupEventFailure(EventType.MessageEvents, EventStatus.failure);
			messageEventService.createGroup(event);
			return ResponseEntity.status(404).body(rm);

		}

	}

	
	 * @SuppressWarnings("rawtypes")
	 * 
	 * @RequestMapping(method = RequestMethod.POST, value = "/api/v1/grouplist")
	 * public ResponseEntity persistGroupList(@RequestBody GroupList groupList)
	 * { boolean flag = messageService.peristGroups(groupList); if (flag) {
	 * rm.message = "GroupList created successfully!!"; rm.messageCode = "200";
	 * Event event =
	 * createMessageEvent.createGroupListEvent(EventType.MessageEvents,
	 * EventStatus.success); messageHandler.persistGroupList(event); return
	 * ResponseEntity.status(200).body(rm); } else { rm.message =
	 * "GroupList creation failed!!"; rm.messageCode = "404"; Event event =
	 * createMessageEvent.createGroupListEvent(EventType.MessageEvents,
	 * EventStatus.failure); messageHandler.persistGroupList(event); return
	 * ResponseEntity.status(404).body(rm);
	 * 
	 * }
	 * 
	 * }
	 

}
*/