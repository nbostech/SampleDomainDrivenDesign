package com.wavelabs.messageapp.services.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wavelabs.messagapp.model.entities.events.MessageHandler;
import com.wavelabs.messagapp.model.entities.events.UserHandler;
import com.wavelabs.messageapp.infrastructure.repositories.MessageHandlerRepository;
import com.wavelabs.messageapp.services.MessageEventService;

@Service
public class MessageEventServiceImpl implements MessageEventService {
	static final Logger log = Logger.getLogger(MessageEventServiceImpl.class);
	@Autowired
	MessageHandlerRepository repo;

	public boolean createMessage(MessageHandler event) {
		try {
			repo.save(event);
			return true;
		} catch (Exception e) {
			log.error(e);
			return false;
		}
	}

	public boolean createMessageInGroup(MessageHandler event) {
		try {
			repo.save(event);
			return true;
		} catch (Exception e) {
			log.error(e);
			return false;
		}
	}

	public boolean createGroup(MessageHandler event) {
		try {
			repo.save(event);
			return true;
		} catch (Exception e) {
			log.error(e);
			return false;
		}
	}

}
