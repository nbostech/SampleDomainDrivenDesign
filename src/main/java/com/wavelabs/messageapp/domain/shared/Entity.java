package com.wavelabs.messageapp.domain.shared;

public interface Entity<T> {
	 boolean sameIdentityAs(T other);
}
