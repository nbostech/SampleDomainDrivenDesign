package com.wavelabs.messageapp.domain.model.group;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.wavelabs.messageapp.domain.model.user.User;


@Entity
@Table(name = "GroupList")
public class GroupList {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private int id;
	@ManyToOne
	@JoinColumn(name = "GroupId")
	private Grouping gid;
	@ManyToOne
	@JoinColumn(name = "MemberId")
	private User mid;

	public GroupList() {

	}

	public GroupList(int id, Grouping gid, User mid) {
		super();
		this.id = id;
		this.gid = gid;
		this.mid = mid;
	}

	public int getId() {
		return id;
	}

	public Grouping getGid() {
		return gid;
	}

	public User getMid() {
		return mid;
	}

}
