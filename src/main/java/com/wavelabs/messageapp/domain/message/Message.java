package com.wavelabs.messageapp.domain.message;

import org.springframework.stereotype.Component;

import com.wavelabs.messageapp.domain.shared.Entity;
@Component
public class Message implements Entity<Message> {
	private MessageId id;
	private UserId toId;
	private UserId fromId;
	private GroupingId gid;
	private Text text;
	@SuppressWarnings("unused")
	private Message message;
	private Grouping grouping;
	public Message(){
		
	}

	public Message(MessageId id, UserId toId, UserId fromId, GroupingId gid, Text text) {
		super();
		this.id = id;
		this.toId = toId;
		this.fromId = fromId;
		this.gid = gid;
		this.text = text;
	}

	public Message message() {
		return message();
	}

	public Grouping grouping() {
		return grouping();
	}

	public void messageCreated(final Message message) {
		this.message = message;

	}

	public void groupMessageCreated(final Message message) {
		this.message = message;
	}

	public void groupCreated(Grouping grouping) {
		this.grouping = grouping;
	}

	public MessageId getId() {
		return id;
	}

	public UserId getToId() {
		return toId;
	}

	public UserId getFromId() {
		return fromId;
	}

	public GroupingId getGid() {
		return gid;
	}

	public Text getText() {
		return text;
	}

	@Override
	public boolean sameIdentityAs(Message other) {
		return false;
	}
	public void createNewMessage(Message message){
		this.message = message;
	}
	public void createNewMessageInGroup(Message message){
		this.message = message;
	}
	public void createNewGroup(Grouping grouping){
		this.grouping = grouping;
	}
}
