package com.wavelabs.messageapp.infrastructure.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.wavelabs.messageapp.domain.model.contacts.Contact;

public interface ContactRepository extends JpaRepository<Contact, Integer> {

	void delete(Contact id);



}
