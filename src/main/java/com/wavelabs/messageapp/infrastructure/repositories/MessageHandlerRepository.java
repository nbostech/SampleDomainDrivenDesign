package com.wavelabs.messageapp.infrastructure.repositories;


import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.wavelabs.messagapp.model.entities.events.MessageHandler;
@Repository
public interface MessageHandlerRepository extends CrudRepository<MessageHandler, Integer> {




}
