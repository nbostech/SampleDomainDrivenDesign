package com.wavelabs.messageapp.services;

import com.wavelabs.messagapp.model.entities.events.UserHandler;

public interface UserEventService {
	 public boolean registerUserEvent(UserHandler event);
	 public boolean registerContactEvent(UserHandler event);
	 public boolean deleteContactEvent(UserHandler event);

}
