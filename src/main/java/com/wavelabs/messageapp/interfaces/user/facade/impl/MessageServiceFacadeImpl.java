package com.wavelabs.messageapp.interfaces.user.facade.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.wavelabs.messageapp.domain.model.group.Grouping;
import com.wavelabs.messageapp.domain.model.message.Message;
import com.wavelabs.messageapp.infrastructure.repositories.GroupRepository;
import com.wavelabs.messageapp.infrastructure.repositories.MessageRepository;
import com.wavelabs.messageapp.interfaces.user.facade.MessageServiceFacade;

@Component
public class MessageServiceFacadeImpl implements MessageServiceFacade{
@Autowired
MessageServiceFacade messageFacade;
@Autowired
MessageRepository messageRepo;
@Autowired
GroupRepository groupRepo;
static final Logger log = Logger.getLogger(MessageServiceFacadeImpl.class);
	public boolean messageCreated(Message message) {
		try{
			messageRepo.save(message);
			return true;
		}catch (Exception e) {
			log.error(e);
			return false;
		}
		
	}
@Override
	public boolean groupmessageCreated(Message message) {
		
		try{
			messageRepo.save(message);
			return true;
		}catch (Exception e) {
			log.error(e);
			return false;
		}
	}
@Override
	public boolean groupCreated(Grouping grouping) {
		try{
			groupRepo.save(grouping);
			return true;
		}catch (Exception e) {
			log.error(e);
			return false;
		}
}	

}
