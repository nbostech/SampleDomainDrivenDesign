package com.wavelabs.messageapp.domain.model.user;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "User")
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private int id;
	@Column(name = "mobile")
	private String mobile;

	public User() {
	}

	public int getId() {
		return id;
	}

	public String getMobile() {
		return mobile;
	}

}
