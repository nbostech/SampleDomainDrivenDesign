package com.wavelabs.messageapp.infrastructure.repositories;

import org.springframework.data.repository.CrudRepository;

import com.wavelabs.messageapp.domain.model.user.User;

public interface UserRepository extends CrudRepository<User, Integer> {


	public User findById(User id);

	public User findById(int id);


	
	
}
