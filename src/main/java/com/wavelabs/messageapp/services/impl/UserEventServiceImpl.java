package com.wavelabs.messageapp.services.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wavelabs.messagapp.model.entities.events.UserHandler;
import com.wavelabs.messageapp.infrastructure.repositories.UserHandlerRepository;
import com.wavelabs.messageapp.services.UserEventService;

@Service
public class UserEventServiceImpl implements UserEventService {
	Logger logger = Logger.getLogger(UserEventServiceImpl.class);
	@Autowired
	private UserHandlerRepository userEventRepository;

	@Override
	public boolean registerUserEvent(UserHandler event) {
		try {
			userEventRepository.save(event);
			return true;
		} catch (Exception e) {
			logger.error(e);
			return false;
		}
	}

	@Override
	public boolean registerContactEvent(UserHandler event) {
		try {
			userEventRepository.save(event);
			return true;
		} catch (Exception e) {
			logger.error(e);
			return false;
		}
	}

	@Override
	public boolean deleteContactEvent(UserHandler event) {
		try {
			userEventRepository.delete(event);
			return true;
		} catch (Exception e) {
			logger.error(e);
			return false;
		}
	}
}
