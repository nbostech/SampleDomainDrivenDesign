package com.wavelabs.messageapp.domain.message;

import com.wavelabs.messageapp.domain.shared.Entity;

public class Grouping implements Entity<Grouping> {
	private GroupingId id;
	private Name name;
	private UserId uid;

	public Grouping(GroupingId id, Name name, UserId uid) {
		super();
		this.id = id;
		this.name = name;
		this.uid = uid;
	}

	public GroupingId getId() {
		return id;
	}

	public Name getName() {
		return name;
	}

	public UserId getUid() {
		return uid;
	}

	@Override
	public boolean sameIdentityAs(Grouping other) {
		return false;
	}
}
