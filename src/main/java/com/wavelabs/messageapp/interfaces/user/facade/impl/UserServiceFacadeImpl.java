package com.wavelabs.messageapp.interfaces.user.facade.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.wavelabs.messageapp.domain.model.contacts.Contact;
import com.wavelabs.messageapp.infrastructure.repositories.ContactRepository;
import com.wavelabs.messageapp.infrastructure.repositories.UserRepository;
import com.wavelabs.messageapp.interfaces.user.facade.UserServiceFacade;

@Component
public class UserServiceFacadeImpl implements UserServiceFacade {
	@Autowired
	UserRepository userRepo;
	@Autowired
	ContactRepository contactRepo;
	@Autowired
	UserServiceFacade userFacade;
	static Logger log = Logger.getLogger(UserServiceFacadeImpl.class);

	@Override
	public boolean addNewContact(Contact contact) {
		try {
			contactRepo.save(contact);
			return true;
		} catch (Exception e) {
			log.error(e);
			return false;
		}

	}

	@Override
	public boolean deleteContact(int id) {
		try {
			contactRepo.delete(id);
			return true;
		} catch (Exception e) {
			log.error(e);
			return false;
		}
	}

}
