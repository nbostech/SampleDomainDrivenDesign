package com.wavelabs.messagapp.model.entities.events;

import java.util.Calendar;

import org.springframework.stereotype.Component;

@Component
public class MessageEventFactory {
	public MessageHandler createmessageEvent(EventType type, EventStatus status) {
		MessageHandler event = new MessageHandler();
		event.setDescription("user created!");
		event.setStatus(EventStatus.success);
		event.setTimeStamp(Calendar.getInstance());
		event.setType(EventType.UserEvents);
		return event;
	}

	public MessageHandler createmessageEventFailure(EventType type, EventStatus status) {
		MessageHandler event = new MessageHandler();
		event.setDescription("user created!");
		event.setStatus(EventStatus.failure);
		event.setTimeStamp(Calendar.getInstance());
		event.setType(EventType.UserEvents);
		return event;
	}

	public MessageHandler createGroupEvent(EventType type, EventStatus status) {
		MessageHandler event = new MessageHandler();
		event.setDescription("group created!");
		event.setStatus(EventStatus.success);
		event.setTimeStamp(Calendar.getInstance());
		event.setType(EventType.MessageEvents);
		return event;
	}

	public MessageHandler createGroupEventFailure(EventType type, EventStatus status) {
		MessageHandler event = new MessageHandler();
		event.setDescription("group creation failed!");
		event.setStatus(EventStatus.failure);
		event.setTimeStamp(Calendar.getInstance());
		event.setType(EventType.MessageEvents);
		return event;
	}

	public MessageHandler createMessageInGroup(EventType type, EventStatus status) {
		MessageHandler event = new MessageHandler();
		event.setDescription("message in group created!");
		event.setStatus(EventStatus.success);
		event.setTimeStamp(Calendar.getInstance());
		event.setType(EventType.MessageEvents);
		return event;
	}

	public MessageHandler createMessageInGroupFailure(EventType type, EventStatus status) {
		MessageHandler event = new MessageHandler();
		event.setDescription("message in group creation fails!!");
		event.setStatus(EventStatus.failure);
		event.setTimeStamp(Calendar.getInstance());
		event.setType(EventType.MessageEvents);
		return event;
	}

}
