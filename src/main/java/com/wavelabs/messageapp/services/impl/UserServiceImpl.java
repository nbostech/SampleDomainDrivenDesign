package com.wavelabs.messageapp.services.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wavelabs.messageapp.domain.model.user.User;
import com.wavelabs.messageapp.infrastructure.repositories.ContactRepository;
import com.wavelabs.messageapp.infrastructure.repositories.UserRepository;
import com.wavelabs.messageapp.services.UserService;

@Service
public class UserServiceImpl implements UserService {
	static Logger log = Logger.getLogger(UserServiceImpl.class);
	@Autowired
	UserService userService;
	@Autowired
	UserRepository userRepository;
	@Autowired
	ContactRepository contactRepo;

	public Boolean createUser(User user) {
		try {
			userRepository.save(user);
			return true;
		} catch (Exception e) {
			log.info(e);
			return false;
		}

	}

}
