package com.wavelabs.messageapp.domain.user;

import com.wavelabs.messageapp.domain.shared.ValueObject;

public class ContactId implements ValueObject<Contact>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public boolean sameValueAs(Contact other) {
		return false;
	}

}
