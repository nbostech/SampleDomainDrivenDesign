package com.wavelabs.messageapp.domain.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wavelabs.messagapp.model.entities.events.EventStatus;
import com.wavelabs.messagapp.model.entities.events.EventType;
import com.wavelabs.messagapp.model.entities.events.UserHandler;
import com.wavelabs.messagapp.model.entities.events.UserHandlerFactory;
import com.wavelabs.messageapp.domain.model.contacts.Contact;
import com.wavelabs.messageapp.domain.model.user.User;
import com.wavelabs.messageapp.interfaces.user.facade.UserServiceFacade;
import com.wavelabs.messageapp.services.UserEventService;
import com.wavelabs.messageapp.services.UserService;

import io.nbos.capi.api.v0.models.RestMessage;

@RestController
@Component
public class UserCommandController {
	@Autowired
	UserService userService;
	@Autowired
	UserHandlerFactory handlingFactory;
	@Autowired
	UserHandler userHandler;
	@Autowired
	UserEventService userEventService;
	@Autowired
	UserServiceFacade userServiceFacade;
	RestMessage rm = new RestMessage();
	@SuppressWarnings("rawtypes")
	@RequestMapping(method = RequestMethod.POST, value = "/api/v1/contact")
	public ResponseEntity addConatct(@RequestBody Contact contactList) {
		boolean flag = userServiceFacade.addNewContact(contactList);

		if (flag) {
		UserHandler event = handlingFactory.registerContactEvent(EventType.UserEvents, EventStatus.success);
			userEventService.registerContactEvent(event);
			rm.message = "Contact created successfully!!";
			rm.messageCode = "200";
			return ResponseEntity.status(200).body(rm);
		} else {
			UserHandler event = handlingFactory.registerContactEventForFailure(EventType.UserEvents,
					EventStatus.failure);
			userEventService.registerContactEvent(event);
			rm.message = "Contact creation failed!!";
			rm.messageCode = "404";
			return ResponseEntity.status(404).body(rm);

		}

	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(method = RequestMethod.DELETE, value = "/v1/api/{Contact_PK_id}")
	public ResponseEntity deleteContact(@RequestParam("id") int id) {
		boolean flag = userServiceFacade.deleteContact(id);

		if (flag) {
			rm.message = "Contact deleted successfully!!";
			rm.messageCode = "200";
			UserHandler event = handlingFactory.deleteContactEvent(EventType.UserEvents, EventStatus.success);
			userEventService.registerContactEvent(event);
			return ResponseEntity.status(200).body(rm);
		} else {
			rm.message = "Contact deletion failed!!";
			rm.messageCode = "404";
			UserHandler event = handlingFactory.deleteContactEventForFailure(EventType.UserEvents,
					EventStatus.failure);
			userEventService.registerContactEvent(event);
			return ResponseEntity.status(404).body(rm);

		}
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(method = RequestMethod.POST, value = "/api/v1/user")
	public ResponseEntity createUser(@RequestBody User user) {
		boolean flag = userService.createUser(user);

		if (flag) {

			rm.message = "User created successfully!!";
			rm.messageCode = "200";
			UserHandler event = handlingFactory.registerUserEvent(EventType.UserEvents, EventStatus.success);
			userEventService.registerUserEvent(event);

			return ResponseEntity.status(200).body(rm);
		} else {
			rm.message = "User creation failed!!";
			rm.messageCode = "404";
			UserHandler event = handlingFactory.registerUserEventForFailure(EventType.UserEvents, EventStatus.failure);
			userEventService.registerUserEvent(event);
			return ResponseEntity.status(404).body(rm);

		}
	}

}
