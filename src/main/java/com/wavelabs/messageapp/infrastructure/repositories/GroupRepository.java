package com.wavelabs.messageapp.infrastructure.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.wavelabs.messageapp.domain.model.group.Grouping;

public interface GroupRepository extends JpaRepository<Grouping, Integer> {

	Grouping findById(int gid);

	void save(com.wavelabs.messageapp.domain.message.Grouping grouping);

}
