package com.wavelabs.messageapp.domain.user;

import com.wavelabs.messageapp.domain.shared.Entity;

public class Contact implements Entity<Contact> {
	private ContactId id;
	private UserId uid;
	private UserId cid;

	public Contact(ContactId id, UserId uid, UserId cid) {
		super();
		this.id = id;
		this.uid = uid;
		this.cid = cid;
	}

	public ContactId getId() {
		return id;
	}

	public UserId getUid() {
		return uid;
	}

	public UserId getCid() {
		return cid;
	}

	@Override
	public boolean sameIdentityAs(Contact other) {
		return false;
	}
}
