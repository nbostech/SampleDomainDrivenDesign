package com.wavelabs.messageapp.domain.model.contacts;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.wavelabs.messageapp.domain.model.user.User;

@Entity
@Table(name = "Contacts")
public class Contact {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private int id;
	@ManyToOne
	@JoinColumn(name = "User_id")
	private User uid;
	@ManyToOne
	@JoinColumn(name = "Cid")
	private User cid;

	public Contact() {

	}

	public Contact(int id, User uid, User cid) {
		super();
		this.id = id;
		this.uid = uid;
		this.cid = cid;
	}

	public int getId() {
		return id;
	}

	public User getUid() {
		return uid;
	}

	public User getCid() {
		return cid;
	}

}
